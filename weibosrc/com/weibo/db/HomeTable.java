package com.weibo.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class HomeTable implements BaseColumns{
	public static final String AUTHORITY = "com.weibo.provider.WeiBoDB";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/home_time_line");
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.weibo.home_time_line";
	public static final String DEFAULT_SORT_ORDER = "time DESC";
	public static final String UID =       "uid";       
	public static final String FAVID =     "favid";     
	public static final String NICK =      "nick";      
	public static final String PORTRAIT =  "portrait";  
	public static final String VIP =       "vip";       
	public static final String CONTENT =   "content";   
	public static final String RTROOTUID = "rtrootuid"; 
	public static final String RTROOTNICK ="rtrootnick";
	public static final String RTPORTRAIT ="rtportrait";
	public static final String RTROOTVIP = "rtrootvip"; 
	public static final String RTREASON =  "rtreason";  
	public static final String RTNUM =     "rtnum";     
	public static final String COMMENTNUM ="commentnum";
	public static final String TIME =      "time";      
	public static final String PIC =       "pic";    
	public static final String OPIC =       "opic";
	public static final String SRC =       "src";       
	public static final String RTROOTID =  "rtrootid";  
	public static final String LONGITUDE = "longitude"; 
	public static final String LATITUDE =  "latitude";      
	public static final String GROUPID  =  "groupid ";  
}
