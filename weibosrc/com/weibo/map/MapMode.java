package com.weibo.map;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.weibo.sina.R;

public class MapMode extends MapActivity
{
    private MapView     mMapView;
    private MapController mMapController; 
    private GeoPoint mGeoPoint;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_view);
        mMapView = (MapView) findViewById(R.id.MapView01);
        //设置为交通模式
        //mMapView.setTraffic(true);
        //设置为卫星模式
        //mMapView.setSatellite(true); 
        //设置为街景模式
        mMapView.setStreetView(false);
        //取得MapController对象(控制MapView)
        mMapController = mMapView.getController(); 
        mMapView.setEnabled(true);
        mMapView.setClickable(true);
        //设置地图支持缩放
        mMapView.setBuiltInZoomControls(true); 
        
        
        //设置起点为 22.538928,113.994162
        mGeoPoint = new GeoPoint((int) (22.538928 * 1000000), (int) (113.994162 * 1000000));
        //定位到深圳
        mMapController.animateTo(mGeoPoint); 
        //设置倍数(1-21)
        mMapController.setZoom(15); 
        
        
        
        //添加Overlay，用于显示标注信息
        MapOverlay myLocationOverlay = new MapOverlay(getBaseContext());
        myLocationOverlay.setGeoPoint(mGeoPoint);
        List<Overlay> list = mMapView.getOverlays();
        list.add(myLocationOverlay);
        Button go = (Button) findViewById(R.id.go);
        go.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Geocoder geoCoder = new Geocoder(MapMode.this, Locale.getDefault());
				EditText edit= (EditText) findViewById(R.id.location); 
				String  location	= edit.getEditableText().toString();
				 try {
			            List<Address> addresses = geoCoder.getFromLocationName(
			            		location, 5);
			            String add = "";
			            if (addresses.size() > 0) {
			            	GeoPoint p = new GeoPoint(
			                        (int) (addresses.get(0).getLatitude() * 1E6), 
			                        (int) (addresses.get(0).getLongitude() * 1E6));
			            	mMapController.animateTo(p);    
			            	mMapView.invalidate();
			            }    
			        } catch (IOException e) {
			            e.printStackTrace();
			        }
				
				
			}
		});
    }
    protected boolean isRouteDisplayed()
    {
        return false;
    }
   
}