
package com.weibo.json;

import java.util.List;

public class Is_follow{
   	private boolean is_follow;
   	private Long trend_id;

 	public boolean getIs_follow(){
		return this.is_follow;
	}
	public void setIs_follow(boolean is_follow){
		this.is_follow = is_follow;
	}
 	public Long getTrend_id(){
		return this.trend_id;
	}
	public void setTrend_id(Long trend_id){
		this.trend_id = trend_id;
	}
}
