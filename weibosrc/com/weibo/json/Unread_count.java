
package com.weibo.json;

import java.util.List;

public class Unread_count{
   	private Long badge;
   	private Long cmt;
   	private Long dm;
   	private Long follower;
   	private Long group;
   	private Long invite;
   	private Long mention_cmt;
   	private Long mention_status;
   	private Long notice;
   	private Long photo;
   	private Long private_group;
   	private Long status;

 	public Long getBadge(){
		return this.badge;
	}
	public void setBadge(Long badge){
		this.badge = badge;
	}
 	public Long getCmt(){
		return this.cmt;
	}
	public void setCmt(Long cmt){
		this.cmt = cmt;
	}
 	public Long getDm(){
		return this.dm;
	}
	public void setDm(Long dm){
		this.dm = dm;
	}
 	public Long getFollower(){
		return this.follower;
	}
	public void setFollower(Long follower){
		this.follower = follower;
	}
 	public Long getGroup(){
		return this.group;
	}
	public void setGroup(Long group){
		this.group = group;
	}
 	public Long getInvite(){
		return this.invite;
	}
	public void setInvite(Long invite){
		this.invite = invite;
	}
 	public Long getMention_cmt(){
		return this.mention_cmt;
	}
	public void setMention_cmt(Long mention_cmt){
		this.mention_cmt = mention_cmt;
	}
 	public Long getMention_status(){
		return this.mention_status;
	}
	public void setMention_status(Long mention_status){
		this.mention_status = mention_status;
	}
 	public Long getNotice(){
		return this.notice;
	}
	public void setNotice(Long notice){
		this.notice = notice;
	}
 	public Long getPhoto(){
		return this.photo;
	}
	public void setPhoto(Long photo){
		this.photo = photo;
	}
 	public Long getPrivate_group(){
		return this.private_group;
	}
	public void setPrivate_group(Long private_group){
		this.private_group = private_group;
	}
 	public Long getStatus(){
		return this.status;
	}
	public void setStatus(Long status){
		this.status = status;
	}
}
