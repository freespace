package com.weibo.json;

import java.util.List;

public class TimeLine {
	private boolean hasvisible;
   	private Long next_cursor;
   	private Long previous_cursor;
   	private List<Statuses> statuses;
   	private Long total_number;

 	public boolean getHasvisible(){
		return this.hasvisible;
	}
	public void setHasvisible(boolean hasvisible){
		this.hasvisible = hasvisible;
	}
 	public Long getNext_cursor(){
		return this.next_cursor;
	}
	public void setNext_cursor(Long next_cursor){
		this.next_cursor = next_cursor;
	}
 	public Long getPrevious_cursor(){
		return this.previous_cursor;
	}
	public void setPrevious_cursor(Long previous_cursor){
		this.previous_cursor = previous_cursor;
	}
 	public List<Statuses> getStatuses(){
		return this.statuses;
	}
	public void setStatuses(List<Statuses> statuses){
		this.statuses = statuses;
	}
 	public Long getTotal_number(){
		return this.total_number;
	}
	public void setTotal_number(Long total_number){
		this.total_number = total_number;
	}

    

}
