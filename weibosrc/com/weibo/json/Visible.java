
package com.weibo.json;


public class Visible{
   	private Long list_id;
   	private Long type;

 	public Long getList_id(){
		return this.list_id;
	}
	public void setList_id(Long list_id){
		this.list_id = list_id;
	}
 	public Long getType(){
		return this.type;
	}
	public void setType(Long type){
		this.type = type;
	}
}
