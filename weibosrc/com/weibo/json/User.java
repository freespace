
package com.weibo.json;


public class User{
   	private boolean allow_all_act_msg;
   	private boolean allow_all_comment;
   	private String avatar_large;
   	private Long bi_followers_count;
   	private String city;
   	private String created_at;
   	private String description;
   	private String domain;
   	private Long favourites_count;
   	private boolean follow_me;
   	private Long followers_count;
   	private boolean following;
   	private Long friends_count;
   	private String gender;
   	private boolean geo_enabled;
   	private Long id;
   	private String idstr;
   	private String lang;
   	private String location;
   	private String name;
   	private Long online_status;
   	private String profile_image_url;
   	private String profile_url;
   	private String province;
   	private String remark;
   	private String screen_name;
   	private Long statuses_count;
   	private String url;
   	private boolean verified;
   	private String verified_reason;
   	private Long verified_type;
   	private String weihao;
   	private Statuses status;

 	public boolean getAllow_all_act_msg(){
		return this.allow_all_act_msg;
	}
	public void setAllow_all_act_msg(boolean allow_all_act_msg){
		this.allow_all_act_msg = allow_all_act_msg;
	}
 	public boolean getAllow_all_comment(){
		return this.allow_all_comment;
	}
	public void setAllow_all_comment(boolean allow_all_comment){
		this.allow_all_comment = allow_all_comment;
	}
 	public String getAvatar_large(){
		return this.avatar_large;
	}
	public void setAvatar_large(String avatar_large){
		this.avatar_large = avatar_large;
	}
 	public Long getBi_followers_count(){
		return this.bi_followers_count;
	}
	public void setBi_followers_count(Long bi_followers_count){
		this.bi_followers_count = bi_followers_count;
	}
 	public String getCity(){
		return this.city;
	}
	public void setCity(String city){
		this.city = city;
	}
 	public String getCreated_at(){
		return this.created_at;
	}
	public void setCreated_at(String created_at){
		this.created_at = created_at;
	}
 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public String getDomain(){
		return this.domain;
	}
	public void setDomain(String domain){
		this.domain = domain;
	}
 	public Long getFavourites_count(){
		return this.favourites_count;
	}
	public void setFavourites_count(Long favourites_count){
		this.favourites_count = favourites_count;
	}
 	public boolean getFollow_me(){
		return this.follow_me;
	}
	public void setFollow_me(boolean follow_me){
		this.follow_me = follow_me;
	}
 	public Long getFollowers_count(){
		return this.followers_count;
	}
	public void setFollowers_count(Long followers_count){
		this.followers_count = followers_count;
	}
 	public boolean getFollowing(){
		return this.following;
	}
	public void setFollowing(boolean following){
		this.following = following;
	}
 	public Long getFriends_count(){
		return this.friends_count;
	}
	public void setFriends_count(Long friends_count){
		this.friends_count = friends_count;
	}
 	public String getGender(){
		return this.gender;
	}
	public void setGender(String gender){
		this.gender = gender;
	}
 	public boolean getGeo_enabled(){
		return this.geo_enabled;
	}
	public void setGeo_enabled(boolean geo_enabled){
		this.geo_enabled = geo_enabled;
	}
 	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id = id;
	}
 	public String getIdstr(){
		return this.idstr;
	}
	public void setIdstr(String idstr){
		this.idstr = idstr;
	}
 	public String getLang(){
		return this.lang;
	}
	public void setLang(String lang){
		this.lang = lang;
	}
 	public String getLocation(){
		return this.location;
	}
	public void setLocation(String location){
		this.location = location;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public Long getOnline_status(){
		return this.online_status;
	}
	public void setOnline_status(Long online_status){
		this.online_status = online_status;
	}
 	public String getProfile_image_url(){
		return this.profile_image_url;
	}
	public void setProfile_image_url(String profile_image_url){
		this.profile_image_url = profile_image_url;
	}
 	public String getProfile_url(){
		return this.profile_url;
	}
	public void setProfile_url(String profile_url){
		this.profile_url = profile_url;
	}
 	public String getProvince(){
		return this.province;
	}
	public void setProvince(String province){
		this.province = province;
	}
 	public String getRemark(){
		return this.remark;
	}
	public void setRemark(String remark){
		this.remark = remark;
	}
 	public String getScreen_name(){
		return this.screen_name;
	}
	public void setScreen_name(String screen_name){
		this.screen_name = screen_name;
	}
 	public Long getStatuses_count(){
		return this.statuses_count;
	}
	public void setStatuses_count(Long statuses_count){
		this.statuses_count = statuses_count;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
 	public boolean getVerified(){
		return this.verified;
	}
	public void setVerified(boolean verified){
		this.verified = verified;
	}
 	public String getVerified_reason(){
		return this.verified_reason;
	}
	public void setVerified_reason(String verified_reason){
		this.verified_reason = verified_reason;
	}
 	public Long getVerified_type(){
		return this.verified_type;
	}
	public void setVerified_type(Long verified_type){
		this.verified_type = verified_type;
	}
 	public String getWeihao(){
		return this.weihao;
	}
	public void setWeihao(String weihao){
		this.weihao = weihao;
	}
    public Statuses getStatus() {
        return status;
    }
    public void setStatus(Statuses status) {
        this.status = status;
    }
}
