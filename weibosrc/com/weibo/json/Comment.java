
package com.weibo.json;


public class Comment{
   	private String created_at;
   	private Long id;
   	private String mid;
   	private String source;
   	private Statuses status;
   	private String text;
   	private User user;
   	private String idstr;

 	public String getCreated_at(){
		return this.created_at;
	}
	public void setCreated_at(String created_at){
		this.created_at = created_at;
	}
 	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id = id;
	}
 	public String getMid(){
		return this.mid;
	}
	public void setMid(String mid){
		this.mid = mid;
	}
 	public String getSource(){
		return this.source;
	}
	public void setSource(String source){
		this.source = source;
	}
 	public Statuses getStatus(){
		return this.status;
	}
	public void setStatus(Statuses status){
		this.status = status;
	}
 	public String getText(){
		return this.text;
	}
	public void setText(String text){
		this.text = text;
	}
 	public User getUser(){
		return this.user;
	}
	public void setUser(User user){
		this.user = user;
	}
    public String getIdstr() {
        return idstr;
    }
    public void setIdstr(String idstr) {
        this.idstr = idstr;
    }
}
