
package com.weibo.json;

import java.util.List;

public class Favorites{
   	private String favorited_time;
   	private Statuses status;
   	private Tags[] tags;

 	public String getFavorited_time(){
		return this.favorited_time;
	}
	public void setFavorited_time(String favorited_time){
		this.favorited_time = favorited_time;
	}
 	public Statuses getStatus(){
		return this.status;
	}
	public void setStatus(Statuses status){
		this.status = status;
	}
 	public Tags[] getTags(){
		return this.tags;
	}
	public void setTags(Tags[] tags){
		this.tags = tags;
	}
}
