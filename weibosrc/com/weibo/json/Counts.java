
package com.weibo.json;

import java.util.List;

public class Counts{
   	private String followers_count;
   	private String friends_count;
   	private String id;
   	private String statuses_count;

 	public String getFollowers_count(){
		return this.followers_count;
	}
	public void setFollowers_count(String followers_count){
		this.followers_count = followers_count;
	}
 	public String getFriends_count(){
		return this.friends_count;
	}
	public void setFriends_count(String friends_count){
		this.friends_count = friends_count;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getStatuses_count(){
		return this.statuses_count;
	}
	public void setStatuses_count(String statuses_count){
		this.statuses_count = statuses_count;
	}
}
