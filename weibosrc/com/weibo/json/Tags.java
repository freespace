
package com.weibo.json;

import java.util.List;

public class Tags{
   	private Long id;
   	private String tag;

 	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id = id;
	}
 	public String getTag(){
		return this.tag;
	}
	public void setTag(String tag){
		this.tag = tag;
	}
}
