
package com.weibo.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect 

@JsonIgnoreProperties (value = { "annotations"})  

public class Statuses{
	private String bmiddle_pic;
   	private Long comments_count;
   	private String created_at;
   	private boolean favorited;
   	private Geo geo;
   	private Long id;
   	private String idstr;
   	private String in_reply_to_screen_name;
   	private String in_reply_to_status_id;
   	private String in_reply_to_user_id;
   	private String mid;
   	private Long mlevel;
   	private Long reposts_count;
   	private String original_pic;
   	private Retweeted_status retweeted_status;
   	private String source;
   	private String text;
   	private boolean truncated;
   	private User user;
   	private Visible visible;
   	private String thumbnail_pic;
   	private Annotation[] annotations;

 	public Long getComments_count(){
		return this.comments_count;
	}
	public void setComments_count(Long comments_count){
		this.comments_count = comments_count;
	}
 	public String getCreated_at(){
		return this.created_at;
	}
	public void setCreated_at(String created_at){
		this.created_at = created_at;
	}
 	public boolean getFavorited(){
		return this.favorited;
	}
	public void setFavorited(boolean favorited){
		this.favorited = favorited;
	}
 	public Geo getGeo(){
		return this.geo;
	}
	public void setGeo(Geo geo){
		this.geo = geo;
	}
 	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id = id;
	}
 	public String getIdstr(){
		return this.idstr;
	}
	public void setIdstr(String idstr){
		this.idstr = idstr;
	}
 	public String getIn_reply_to_screen_name(){
		return this.in_reply_to_screen_name;
	}
	public void setIn_reply_to_screen_name(String in_reply_to_screen_name){
		this.in_reply_to_screen_name = in_reply_to_screen_name;
	}
 	public String getIn_reply_to_status_id(){
		return this.in_reply_to_status_id;
	}
	public void setIn_reply_to_status_id(String in_reply_to_status_id){
		this.in_reply_to_status_id = in_reply_to_status_id;
	}
 	public String getIn_reply_to_user_id(){
		return this.in_reply_to_user_id;
	}
	public void setIn_reply_to_user_id(String in_reply_to_user_id){
		this.in_reply_to_user_id = in_reply_to_user_id;
	}
 	public String getMid(){
		return this.mid;
	}
	public void setMid(String mid){
		this.mid = mid;
	}
 	public Long getMlevel(){
		return this.mlevel;
	}
	public void setMlevel(Long mlevel){
		this.mlevel = mlevel;
	}
 	public Long getReposts_count(){
		return this.reposts_count;
	}
	public void setReposts_count(Long reposts_count){
		this.reposts_count = reposts_count;
	}
 	public Retweeted_status getRetweeted_status(){
		return this.retweeted_status;
	}
	public void setRetweeted_status(Retweeted_status retweeted_status){
		this.retweeted_status = retweeted_status;
	}
 	public String getSource(){
		return this.source;
	}
	public void setSource(String source){
		this.source = source;
	}
 	public String getText(){
		return this.text;
	}
	public void setText(String text){
		this.text = text;
	}
 	public boolean getTruncated(){
		return this.truncated;
	}
	public void setTruncated(boolean truncated){
		this.truncated = truncated;
	}
 	public User getUser(){
		return this.user;
	}
	public void setUser(User user){
		this.user = user;
	}
 	public Visible getVisible(){
		return this.visible;
	}
	public void setVisible(Visible visible){
		this.visible = visible;
	}
	public String getThumbnail_pic() {
		return thumbnail_pic;
	}
	public void setThumbnail_pic(String thumbnail_pic) {
		this.thumbnail_pic = thumbnail_pic;
	}
	public String getBmiddle_pic() {
		return bmiddle_pic;
	}
	public void setBmiddle_pic(String bmiddle_pic) {
		this.bmiddle_pic = bmiddle_pic;
	}
	public String getOriginal_pic() {
		return original_pic;
	}
	public void setOriginal_pic(String original_pic) {
		this.original_pic = original_pic;
	}
	public Annotation[] getAnnotations() {
		return annotations;
	}
	public void setAnnotations(Annotation[] annotations) {
		this.annotations = annotations;
	}
}
