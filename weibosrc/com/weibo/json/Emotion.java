
package com.weibo.json;

import java.util.List;

public class Emotion{
   	private String category;
   	private boolean common;
   	private boolean hot;
   	private String icon;
   	private String phrase;
   	private String picid;
   	private String type;
   	private String url;
   	private String value;

 	public String getCategory(){
		return this.category;
	}
	public void setCategory(String category){
		this.category = category;
	}
 	public boolean getCommon(){
		return this.common;
	}
	public void setCommon(boolean common){
		this.common = common;
	}
 	public boolean getHot(){
		return this.hot;
	}
	public void setHot(boolean hot){
		this.hot = hot;
	}
 	public String getIcon(){
		return this.icon;
	}
	public void setIcon(String icon){
		this.icon = icon;
	}
 	public String getPhrase(){
		return this.phrase;
	}
	public void setPhrase(String phrase){
		this.phrase = phrase;
	}
 	public String getPicid(){
		return this.picid;
	}
	public void setPicid(String picid){
		this.picid = picid;
	}
 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
 	public String getValue(){
		return this.value;
	}
	public void setValue(String value){
		this.value = value;
	}
}
