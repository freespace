
package com.weibo.json;

import java.util.List;

public class Privacy{
   	private Long badge;
   	private Long comment;
   	private Long geo;
   	private Long message;
   	private Long mobile;
   	private Long realname;

 	public Long getBadge(){
		return this.badge;
	}
	public void setBadge(Long badge){
		this.badge = badge;
	}
 	public Long getComment(){
		return this.comment;
	}
	public void setComment(Long comment){
		this.comment = comment;
	}
 	public Long getGeo(){
		return this.geo;
	}
	public void setGeo(Long geo){
		this.geo = geo;
	}
 	public Long getMessage(){
		return this.message;
	}
	public void setMessage(Long message){
		this.message = message;
	}
 	public Long getMobile(){
		return this.mobile;
	}
	public void setMobile(Long mobile){
		this.mobile = mobile;
	}
 	public Long getRealname(){
		return this.realname;
	}
	public void setRealname(Long realname){
		this.realname = realname;
	}
}
