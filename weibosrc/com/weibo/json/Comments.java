
package com.weibo.json;


public class Comments {
    
    private Comment[] comments;

    private Long next_cursor;

    private Long previous_cursor;
    
    private Long total_number;
    
    private boolean hasvisible;

    public Long getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(Long next_cursor) {
        this.next_cursor = next_cursor;
    }

    public Long getPrevious_cursor() {
        return previous_cursor;
    }

    public void setPrevious_cursor(Long previous_cursor) {
        this.previous_cursor = previous_cursor;
    }

    public Long getTotal_number() {
        return total_number;
    }

    public void setTotal_number(Long total_number) {
        this.total_number = total_number;
    }

    

    public Comment[] getComments() {
        return comments;
    }

    public void setComments(Comment[] comments) {
        this.comments = comments;
    }

    public boolean isHasvisible() {
        return hasvisible;
    }

    public void setHasvisible(boolean hasvisible) {
        this.hasvisible = hasvisible;
    }

}
