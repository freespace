
package com.weibo.json;

import java.util.List;

public class Trends{
   	private String hotword;
   	private Long num;
   	private Long trend_id;

 	public String getHotword(){
		return this.hotword;
	}
	public void setHotword(String hotword){
		this.hotword = hotword;
	}
 	public Long getNum(){
		return this.num;
	}
	public void setNum(Long num){
		this.num = num;
	}
 	public Long getTrend_id(){
		return this.trend_id;
	}
	public void setTrend_id(Long trend_id){
		this.trend_id = trend_id;
	}
}
