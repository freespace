
package com.weibo.sina;

import java.util.ArrayList;
import java.util.List;

import com.weibo.json.Comment;
import com.weibo.json.Comments;
import com.weibo.json.Reposts;
import com.weibo.client.Weibo;
import android.app.TabActivity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.weibo.json.Statuses;
import com.weibo.util.AsyncImageLoader;
import com.weibo.view.StatusView;

public class MessageDetail extends TabActivity {

    private TextView reply_title;

    private TextView retweet_title;

    private ImageView mImageView;

    private Drawable mDrawable = null;

    private static final int UPDATEFACE = 0x00;

    private static final int UPDATECONTENT = 0x01;

    private static final String TAG = "MessageDetail";

    private List<String> mComments;

    private ArrayAdapter<String> mReplyAdapter;

    private List<String> mRetweet;

    private ArrayAdapter<String> mRetweetAdapter;

    private Statuses mStatuses;

    private long mMid;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATEFACE: // 更新用户头像
                    mImageView.setBackgroundDrawable(mDrawable);
                    mImageView.invalidate();
                    break;
                case UPDATECONTENT: // 更新用户评论，转发数
                    reply_title.setText(String.format(getString(R.string.reply_number), msg
                            .getData().getLong("replyCount")));
                    retweet_title.setText(String.format(getString(R.string.retweet_number), msg
                            .getData().getLong("retweetCount")));
                    if (msg.getData().getLong("replyCount") > 0
                            || msg.getData().getLong("retweetCount") > 0) {
                        mHandler.post(new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Comments comments = Weibo.getInstance().getComments(mMid + "");

                                    for (Comment comment : comments.getComments()) {
                                        mComments.add(comment.getText());
                                    }
                                    mReplyAdapter.notifyDataSetChanged();
                                    Reposts reposts = Weibo.getInstance().getReposttimeline(
                                            mMid + "");
                                    for (Statuses repost : reposts.getReposts()) {
                                        mRetweet.add(repost.getText());
                                    }
                                    mRetweetAdapter.notifyDataSetChanged();
                                } catch (com.weibo.net.WeiboException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                ProgressBar proBar = (ProgressBar) findViewById(R.id.updateProgressBar);
                                proBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    } else {
                        ProgressBar proBar = (ProgressBar) findViewById(R.id.updateProgressBar);
                        proBar.setVisibility(View.INVISIBLE);
                    }

                    break;
            }
            super.handleMessage(msg);
        }

    };

    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.message_detail);
        super.onCreate(savedInstanceState);
        TabHost tabHost = getTabHost();
        LayoutInflater.from(this).inflate(R.layout.message_detail_tab_body,
                tabHost.getTabContentView(), true);
        TabWidget tw = tabHost.getTabWidget();
        LinearLayout reply = (LinearLayout) LayoutInflater.from(this).inflate(
                R.layout.tab_indicator, tw, false);
        reply_title = (TextView) reply.getChildAt(0);
        reply_title.setText(String.format(getString(R.string.reply_number), "*"));
        LinearLayout retweet = (LinearLayout) LayoutInflater.from(this).inflate(
                R.layout.tab_indicator, tw, false);
        retweet_title = (TextView) retweet.getChildAt(0);
        retweet_title.setText(String.format(getString(R.string.reply_number), "*"));
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(reply).setContent(R.id.reply_list));
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(retweet)
                .setContent(R.id.retweet_list));
        StatusView statusView = (StatusView) findViewById(R.id.status_view);
        statusView.getUser_name().setText(getIntent().getStringExtra("name"));
        statusView.getContent().setText(getIntent().getStringExtra("content"));
        mImageView = statusView.getFace();
        mImageView.setBackgroundResource(R.drawable.face);

        ListView replyListView = (ListView) findViewById(R.id.reply_list);
        mComments = new ArrayList<String>();
        mReplyAdapter = new ArrayAdapter<String>(getBaseContext(),
                android.R.layout.simple_list_item_1, mComments);
        replyListView.setAdapter(mReplyAdapter);
        replyListView = (ListView) findViewById(R.id.retweet_list);
        mRetweet = new ArrayList<String>();
        mRetweetAdapter = new ArrayAdapter<String>(getBaseContext(),
                android.R.layout.simple_list_item_1, mRetweet);
        replyListView.setAdapter(mRetweetAdapter);
        new LoadingThread(getIntent().getStringExtra("face_url")).start();
        new ContentThread().start();

    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    private class LoadingThread extends Thread {

        String mUrl;

        LoadingThread(String url) {
            mUrl = url;
        }

        @Override
        public void run() {
            mDrawable = AsyncImageLoader.loadImageFromUrl(getBaseContext(), mUrl);
            mHandler.obtainMessage(UPDATEFACE).sendToTarget();
            super.run();
        }

    }

    private class ContentThread extends Thread {

        /*
         * (non-Javadoc)
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            mMid = getIntent().getLongExtra("id", 0);
            // List<Comment> comments = weibo.getComments(id);
            // Log.e(TAG, "Comment = " + comments.get(0).toString());
            try {
                mStatuses = Weibo.getInstance().showStatus(mMid);
            } catch (com.weibo.net.WeiboException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putLong("replyCount", mStatuses.getComments_count());
            bundle.putLong("retweetCount", mStatuses.getReposts_count());
            msg.setData(bundle);
            msg.what = UPDATECONTENT;
            mHandler.sendMessage(msg);
            super.run();
        }

    }
}
