
package com.weibo.sina;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.weibo.client.Weibo;
import com.weibo.json.Statuses;
import com.weibo.net.WeiboException;
import com.weibo.view.StatusAdapter;

public class Mentions extends Activity {
    static final String TAG = "Mentions";

    private List<Statuses> mTimeLine;

    private ListView mTimelineView;

    private StatusAdapter mAdapter;

    private static final int UPDATELIST = 0x00;

    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATELIST:
                    Log.e(TAG, "update list ");
                    if (mAdapter != null)
                        mAdapter.notifyDataSetChanged();
                    if (mTimelineView != null)
                        mTimelineView.invalidate();
                    break;
            }
        }
    };

    private View mUpdateView;

    private EditText mUpdate_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.mentions);
        mTimeLine = new ArrayList<Statuses>();
        setupPages();
        super.onCreate(savedInstanceState);
    }

    private void setupPages() {

        mHandler.post(new UpdateTLThread());
    }

    class UpdateTLThread extends Thread {

        @Override
        public void run() {
            getTimeLine(1);
            mTimelineView = (ListView) findViewById(R.id.timeline);
            mTimelineView.setFocusable(false);
            mTimelineView.setClickable(false);
            mTimelineView.setOnItemClickListener(null);
            mAdapter = new StatusAdapter(Mentions.this, R.layout.status_view, mTimeLine,
                    mTimelineView);
            mTimelineView.setAdapter(mAdapter);
            super.run();
        }

    }

    private void getTimeLine(int page) {
        Log.e(TAG, "get page =" + page);
        mTimeLine.clear();
        try {
            mTimeLine.addAll(Weibo.getInstance().getMentions().getStatuses());
        } catch (WeiboException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mHandler.obtainMessage(UPDATELIST).sendToTarget();
    }

    public void handleBtn(View view) {
        switch (view.getId()) {
            case R.id.refresh_btn:
                getTimeLine(1);
                break;
            case R.id.home_btn:
                startActivity(new Intent(this, Face.class));
                break;
            case R.id.tweet_btn:
                if (mUpdateView == null) {
                    ViewStub tmp = (ViewStub) findViewById(R.id.post_message);
                    mUpdateView = (View) tmp.inflate();
                    mUpdate_edit = (EditText) mUpdateView.findViewById(R.id.post_edit);
                    mUpdate_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            Log.e(TAG, "onFocuseChange");
                            if (hasFocus) {
                                Log.e(TAG, "get Focuse");
                                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                if (imm != null) {

                                    imm.showSoftInput(mUpdate_edit, InputMethodManager.SHOW_FORCED);
                                }
                            } else {
                                Log.e(TAG, "lost Focuse");
                                if (Mentions.this.getCurrentFocus() != null) {
                                    Log.e(TAG, "hideSoftInputFromWindow");
                                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                            .hideSoftInputFromWindow(Mentions.this
                                                    .getCurrentFocus().getWindowToken(),
                                                    InputMethodManager.HIDE_NOT_ALWAYS);
                                }
                            }
                        }
                    });

                    Button btn = (Button) mUpdateView.findViewById(R.id.update_button);
                    btn.setOnClickListener(new OnClickListener() {

                        public void onClick(View v) {
                            try {
                                Weibo.getInstance().updateStatus(
                                        mUpdate_edit.getEditableText().toString(), null, null,
                                        null, false);
                            } catch (WeiboException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            Animation anim = AnimationUtils.loadAnimation(Mentions.this,
                                    R.anim.update_view_out);
                            anim.setAnimationListener(new AnimationListener() {

                                public void onAnimationStart(Animation animation) {
                                    // TODO Auto-generated method stub

                                }

                                public void onAnimationRepeat(Animation animation) {
                                    // TODO Auto-generated method stub

                                }

                                public void onAnimationEnd(Animation animation) {
                                    Log.e(TAG, "onAnimationEnd");
                                    mUpdate_edit.setText("");
                                    if (Mentions.this.getCurrentFocus() != null) {
                                        Log.e(TAG, "hideSoftInputFromWindow");
                                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                                .hideSoftInputFromWindow(Mentions.this
                                                        .getCurrentFocus().getWindowToken(),
                                                        InputMethodManager.HIDE_NOT_ALWAYS);
                                    }
                                    mUpdateView.setVisibility(View.GONE);
                                }
                            });
                            mUpdateView.startAnimation(anim);

                        }
                    });
                    btn = (Button) mUpdateView.findViewById(R.id.cancel_button);
                    btn.setOnClickListener(new OnClickListener() {

                        public void onClick(View v) {
                            Animation anim = AnimationUtils.loadAnimation(Mentions.this,
                                    R.anim.update_view_out);
                            anim.setAnimationListener(new AnimationListener() {

                                public void onAnimationStart(Animation animation) {
                                    // TODO Auto-generated method stub

                                }

                                public void onAnimationRepeat(Animation animation) {
                                    // TODO Auto-generated method stub

                                }

                                public void onAnimationEnd(Animation animation) {
                                    Log.e(TAG, "onAnimationEnd");
                                    mUpdate_edit.setText("");
                                    if (Mentions.this.getCurrentFocus() != null) {
                                        Log.e(TAG, "hideSoftInputFromWindow");
                                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                                .hideSoftInputFromWindow(Mentions.this
                                                        .getCurrentFocus().getWindowToken(),
                                                        InputMethodManager.HIDE_NOT_ALWAYS);
                                    }
                                    mUpdateView.setVisibility(View.GONE);
                                }
                            });
                            mUpdateView.startAnimation(anim);

                        }
                    });
                }
                mUpdateView.setVisibility(View.VISIBLE);
                mUpdate_edit.requestFocus();
                mUpdateView.startAnimation(AnimationUtils
                        .loadAnimation(this, R.anim.update_view_in));
                break;
        }
    }

}
