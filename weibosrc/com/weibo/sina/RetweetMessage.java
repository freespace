
package com.weibo.sina;

import com.weibo.client.Weibo;
import com.weibo.net.WeiboException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RetweetMessage extends Activity {
    private static final String TAG = "RetweetMessage";

    private EditText mUpdate_edit;

    private static final int RETWEET = 0x00;

    private long mId;

    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RETWEET:
                    String tmpString = mUpdate_edit.getEditableText().toString();
                    tmpString = tmpString.trim();
                    if (tmpString.length() == 0) {
                        Toast.makeText(getBaseContext(), R.string.content_empty, Toast.LENGTH_LONG)
                                .show();
                        return;
                    }
                    if (tmpString.length() > 140) {
                        Toast.makeText(getBaseContext(), R.string.content_outnumber,
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    try {
                        Weibo.getInstance().repost(mUpdate_edit.getEditableText().toString(),
                                mId + "");
                    } catch (WeiboException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    // ProgressBar progressBar = (ProgressBar)
                    // findViewById(R.id.updateProgressBar);
                    // progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getBaseContext(), R.string.tips_send_ok, Toast.LENGTH_LONG)
                            .show();
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.reply_layout);
        super.onCreate(savedInstanceState);
        mUpdate_edit = (EditText) findViewById(R.id.post_edit);

        mUpdate_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                // Log.e(TAG, "onFocuseChange");
                if (hasFocus) {
                    Log.e(TAG, "get Focuse");
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    if (imm != null) {

                        imm.showSoftInput(mUpdate_edit, InputMethodManager.SHOW_FORCED);
                    }
                } else {
                    Log.e(TAG, "lost Focuse");
                    if (RetweetMessage.this.getCurrentFocus() != null) {
                        // Log.e(TAG, "hideSoftInputFromWindow");
                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(RetweetMessage.this.getCurrentFocus()
                                        .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
            }
        });

        Button btn = (Button) findViewById(R.id.update_button);
        btn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                // ProgressBar progressBar = (ProgressBar)
                // findViewById(R.id.updateProgressBar);
                // progressBar.setVisibility(View.VISIBLE);
                mHandler.obtainMessage(RETWEET).sendToTarget();

            }
        });
        btn = (Button) findViewById(R.id.cancel_button);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
                Log.e(TAG, "finish()");
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        mUpdate_edit.requestFocus();
        View reply = (View) findViewById(R.id.reply_view);
        reply.setVisibility(View.VISIBLE);
        TextView nick = (TextView) findViewById(R.id.nick);
        Log.e(TAG, "name=" + getIntent().getStringExtra("name"));
        Log.e(TAG, "content=" + getIntent().getStringExtra("content"));
        nick.setText(getIntent().getStringExtra("name"));
        nick = (TextView) findViewById(R.id.content);
        nick.setText(getIntent().getStringExtra("content"));
        mId = getIntent().getLongExtra("id", 0);
    }

}
