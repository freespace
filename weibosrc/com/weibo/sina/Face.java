package com.weibo.sina;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ViewFlipper;

public class Face extends Activity {

	private ViewFlipper mFlipper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.face);
		mFlipper = ((ViewFlipper) this.findViewById(R.id.flipper));
		mFlipper.setAutoStart(false);
		ImageButton right = ((ImageButton) this.findViewById(R.id.right_arrow));
		right.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				mFlipper.setInAnimation(AnimationUtils.loadAnimation(Face.this,R.anim.push_left_in));
				mFlipper.setOutAnimation(AnimationUtils.loadAnimation(Face.this,R.anim.push_left_out));
				mFlipper.showNext();
				v.setVisibility(View.INVISIBLE);
				findViewById(R.id.left_arrow).setVisibility(View.VISIBLE);
			}
		});
		ImageButton left = ((ImageButton) this.findViewById(R.id.left_arrow));
		left.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				mFlipper.setInAnimation(AnimationUtils.loadAnimation(Face.this,R.anim.push_right_in));
				mFlipper.setOutAnimation(AnimationUtils.loadAnimation(Face.this,R.anim.push_right_out));
				mFlipper.showNext();
				v.setVisibility(View.INVISIBLE);
				findViewById(R.id.right_arrow).setVisibility(View.VISIBLE);
			}
		});
	}
	
	
    public void onCAButtonClick(View view){
    	switch(view.getId()){
    	case R.id.timeline_btn:
    		startActivity(new Intent(this,TimeLineView.class));
    		break;
    	case R.id.mentions_btn:
    		startActivity(new Intent(this,Mentions.class));
    		break;
    	case R.id.direct_message_btn:
    		startActivity(new Intent(this,DirectMessages.class));
    		break;
    	case R.id.mu_profile_btn:
    		startActivity(new Intent(this,Profile.class));
    		break;
    	}

    }
}
