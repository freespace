package com.weibo.sina;

import com.weibo.client.Weibo;
import com.weibo.net.AccessToken;
import com.weibo.net.DialogError;
import com.weibo.net.WeiboDialogListener;
import com.weibo.net.WeiboException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Authorize extends Activity {

	static final String TAG = "Authorize";
	static final String FACEACTIVITY = "app://TimeLineActivity";
	private AnimationDrawable mFrameAnimation;
	private Handler mHandle;
	
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// ImageView img = (ImageView)findViewById(R.id.logo_bo);
		// img.setBackgroundResource(R.drawable.bo);
		// mFrameAnimation = (AnimationDrawable) img.getBackground();
		// Log.e(TAG, " frameAnimation.start()" );
		// (new Timer(false)).schedule(new AnimationTimer(mFrameAnimation),
		// 100);

	}

	@Override
	protected void onResume() {
		Log.e(TAG, "onResume");
		if (!checkNetworkInfo()) {
			showDialog(0);
		} else {
			Thread initThread = new Thread() {

				@Override
				public void run() {
					SharedPreferences user = getSharedPreferences("oauth", 0);
					String oauth_token = user.getString("oauth_token", "");
					String oauth_token_secret = user.getString("oauth_token_secret", "");
					if (oauth_token.length() > 0 && oauth_token_secret.length() > 0) {
					    AccessToken accessToken = new AccessToken(oauth_token, Weibo.CONSUMER_SECRET);
			            accessToken.setExpiresIn(oauth_token_secret);
			            Weibo.getInstance().setAccessToken(accessToken);
			            Weibo.getInstance().setContext(Authorize.this.getBaseContext());
			            Weibo.setUid(user.getString("uid", ""));
			            Intent intent = new Intent();
			            intent.setClass(Authorize.this, TimeLineView.class);
			            startActivity(intent);
						finish();
						return;
					} else {
						Weibo weibo = Weibo.getInstance();
						weibo.setContext(Authorize.this.getBaseContext());
                        weibo.setupConsumerConfig(Weibo.CONSUMER_KEY, Weibo.CONSUMER_SECRET);

                        // Oauth2.0
                        // 隐式授权认证方式
                        weibo.setRedirectUrl("http://dawenxi.sinaapp.com/callback.php");// 此处回调页内容应该替换为与appkey对应的应用回调页
                        // 对应的应用回调页可在开发者登陆新浪微博开发平台之后，
                        // 进入我的应用--应用详情--应用信息--高级信息--授权设置--应用回调页进行设置和查看，
                        // 应用回调页不可为空

                        weibo.authorize(Authorize.this,
                                new AuthDialogListener());
						Button beginOuathBtn = (Button) findViewById(R.id.getAccessToken);
						beginOuathBtn.setVisibility(View.VISIBLE);
						beginOuathBtn.setOnClickListener(new Button.OnClickListener() {

							public void onClick(View v) {
							    Weibo weibo = Weibo.getInstance();
							    Weibo.getInstance().setContext(Authorize.this.getBaseContext());
                                weibo.setupConsumerConfig(Weibo.CONSUMER_KEY, Weibo.CONSUMER_SECRET);

                                // Oauth2.0
                                // 隐式授权认证方式
                                weibo.setRedirectUrl("http://dawenxi.sinaapp.com/callback.php");// 此处回调页内容应该替换为与appkey对应的应用回调页
                                // 对应的应用回调页可在开发者登陆新浪微博开发平台之后，
                                // 进入我的应用--应用详情--应用信息--高级信息--授权设置--应用回调页进行设置和查看，
                                // 应用回调页不可为空

                                weibo.authorize(Authorize.this,
                                        new AuthDialogListener());
							}
						});

					}

					super.run();
				}

			};
			mHandle = new Handler();
			mHandle.postDelayed(initThread, 1000);

		}
		super.onResume();

	}

	// private static class AnimationTimer extends TimerTask {
	// AnimationDrawable animation;
	//
	// public AnimationTimer(AnimationDrawable animation) {
	// this.animation = animation;
	// }
	//
	// @Override
	// public void run() {
	// animation.start();
	// this.cancel();
	// }
	//
	// }

	private boolean checkNetworkInfo() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		// mobile 3G Data Network
		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
		// wifi
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		// 如果3G网络和wifi网络都未连接，且不是处于正在连接状态 则进入Network Setting界面 由用户配置网络连接
		if (mobile == State.CONNECTED || mobile == State.CONNECTING)
			return true;
		if (wifi == State.CONNECTED || wifi == State.CONNECTING)
			return true;
		return false;
		// 进入无线网络配置界面
		// startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
		// //进入手机中的wifi网络设置界面
	}

	protected Dialog onCreateDialog(int id, Bundle args) {
		return new AlertDialog.Builder(Authorize.this).setTitle(getString(R.string.net_not_connect))
				.setPositiveButton(R.string.set_net, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
					}
				}).setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
				}).create();
	}
	
class AuthDialogListener implements WeiboDialogListener {

        
        public void onComplete(Bundle values) {
            String token = values.getString("access_token");
            String expires_in = values.getString("expires_in");
            Log.e(TAG, values.getString("uid"));
            Weibo.setUid(values.getString("uid"));
            SharedPreferences user = getSharedPreferences("oauth", 0);
            Editor editor = user.edit();
            editor.putString("oauth_token", token);
            editor.putString("oauth_token_secret", expires_in);
            editor.putString("uid", values.getString("uid"));
            editor.commit();
            AccessToken accessToken = new AccessToken(token, Weibo.CONSUMER_SECRET);
            accessToken.setExpiresIn(expires_in);
            Weibo.getInstance().setAccessToken(accessToken);
            Intent intent = new Intent();
            intent.setClass(Authorize.this, TimeLineView.class);
            startActivity(intent);
            finish();
        }

        
        public void onError(DialogError e) {
            Toast.makeText(getApplicationContext(),
                    "Auth error : " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        
        public void onCancel() {
            Toast.makeText(getApplicationContext(), "Auth cancel",
                    Toast.LENGTH_LONG).show();
        }

        
        public void onWeiboException(WeiboException e) {
            Toast.makeText(getApplicationContext(),
                    "Auth exception : " + e.getMessage(), Toast.LENGTH_LONG)
                    .show();
        }

    }

}
