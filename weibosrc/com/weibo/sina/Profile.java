
package com.weibo.sina;

import java.util.Date;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.widget.ImageView;
import android.widget.TextView;

import com.weibo.client.Weibo;
import com.weibo.json.User;
import com.weibo.net.WeiboException;
import com.weibo.util.MD5;
import com.weibo.util.Util;

public class Profile extends Activity {
    private User user;

    private final static int UPDATEFACE = 0x00;

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATEFACE:
                    ImageView face = (ImageView) findViewById(R.id.face);
                    face.setImageDrawable((Drawable) msg.obj);
                    break;
            }
            super.handleMessage(msg);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.profile);
        try {
            user = Weibo.getInstance().userShow(Long.valueOf(Weibo.getUid()));
        } catch (WeiboException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ImageView face = (ImageView) findViewById(R.id.face);
        String key = MD5.getMD5(user.getProfile_image_url().getBytes());
        Drawable imageDrawable = Util.loadImage(key, user.getProfile_image_url(), getBaseContext(),
                mHandler, UPDATEFACE);
        if (imageDrawable != null)
            face.setBackgroundDrawable(imageDrawable);
        else {
            face.setBackgroundResource(R.drawable.face);
        }
        TextView textView = (TextView) findViewById(R.id.nice_name);
        textView.setText(user.getName());
        textView = (TextView) findViewById(R.id.create_time);
        String today = DateFormat.format(
                getBaseContext().getString(R.string.full_wday_month_day_no_year),
                new Date(user.getCreated_at())).toString();
        textView.setText(today);

        textView = (TextView) findViewById(R.id.fance_count);
        String count = getResources().getString(R.string.fance_count);
        count = String.format(count, user.getFriends_count());
        textView.setText(count);

        textView = (TextView) findViewById(R.id.follow_count);
        count = getResources().getString(R.string.follow_count);
        count = String.format(count, user.getFollowers_count());
        textView.setText(count);

        textView = (TextView) findViewById(R.id.favourites_count);
        count = getResources().getString(R.string.favourites_count);
        count = String.format(count, user.getFavourites_count());
        textView.setText(count);

        textView = (TextView) findViewById(R.id.description);
        textView.setText(user.getDescription());

        textView = (TextView) findViewById(R.id.address);
        textView.setText(user.getLocation());

        textView = (TextView) findViewById(R.id.blog_url);
        if (user.getUrl() != null)
            textView.setText(user.getUrl().toString());
        super.onCreate(savedInstanceState);
    }
}
