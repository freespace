package com.weibo.sina;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.weibo.client.Weibo;
import com.weibo.net.WeiboException;

public class PostMassage extends Activity {
	private static final String TAG="PostMassage";
	private EditText mUpdate_edit;
	private static final int UPDATE=0x00;
	private final Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
		switch(msg.what){
		case UPDATE:
			try {
				Weibo.getInstance().updateStatus(mUpdate_edit.getEditableText().toString(),null,null,null,false);
			} catch (WeiboException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
//			ProgressBar progressBar = (ProgressBar) findViewById(R.id.updateProgressBar);
//			progressBar.setVisibility(View.INVISIBLE);
			Toast.makeText(getBaseContext(), "update OK", Toast.LENGTH_LONG).show();
			finish();
			overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		}
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.post_message);
		super.onCreate(savedInstanceState);
		mUpdate_edit = (EditText) findViewById(R.id.post_edit);
		
		mUpdate_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				// Log.e(TAG, "onFocuseChange");
				if (hasFocus) {
					  Log.e(TAG, "get Focuse");
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					if (imm != null) {

						imm.showSoftInput(mUpdate_edit, InputMethodManager.SHOW_FORCED);
					}
				} else {
					Log.e(TAG, "lost Focuse");
					if (PostMassage.this.getCurrentFocus() != null) {
						// Log.e(TAG, "hideSoftInputFromWindow");
						((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(PostMassage.this
								.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					}
				}
			}
		});

		Button btn = (Button) findViewById(R.id.update_button);
		btn.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
//					ProgressBar progressBar = (ProgressBar) findViewById(R.id.updateProgressBar);
//					progressBar.setVisibility(View.VISIBLE);
					mHandler.obtainMessage(UPDATE).sendToTarget();

			}
		});
		btn = (Button) findViewById(R.id.cancel_button);
		btn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
				Log.e(TAG, "finish()");
				overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
			}
		});
		mUpdate_edit.requestFocus();
	}

}
