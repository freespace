package com.weibo.sina;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.weibo.view.DirectMessageAdapter;

public class DirectMessages extends Activity {
	static final String TAG = "DirectMessage";
	//private List<DirectMessage> mTimeLine;
	private ListView mTimelineView;
	private DirectMessageAdapter mAdapter;
	private static final int UPDATELIST = 0x00;
	
	private final Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case UPDATELIST:
				Log.e(TAG, "update list ");
				if (mAdapter != null)
					//mAdapter.notifyDataSetChanged();
				if (mTimelineView != null)
					mTimelineView.invalidate();
				break;
			}
		}
	};
	private View mUpdateView;
	private EditText mUpdate_edit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.direct_message);
		//mTimeLine = new ArrayList<DirectMessage>();
		setupPages();
		super.onCreate(savedInstanceState);
	}
	private void setupPages() {

		mHandler.post(new UpdateTLThread());
	}
	class UpdateTLThread extends Thread {

		@Override
		public void run() {
			getTimeLine(1);
			mTimelineView = (ListView)findViewById(R.id.timeline);
			mTimelineView.setFocusable(false);
			mTimelineView.setClickable(false);
			mTimelineView.setOnItemClickListener(null);
			//mAdapter = new DirectMessageAdapter(DirectMessages.this, R.layout.direct_message_view, mTimeLine, mTimelineView);
			//mTimelineView.setAdapter(mAdapter);
			super.run();
		}

	}
	private void getTimeLine(int page) {
		Log.e(TAG, "get page =" + page);
		//mTimeLine.clear();
		//mTimeLine.addAll(weibo.getDirectMessages(new Paging(page)));
		mHandler.obtainMessage(UPDATELIST).sendToTarget();
	}
	
	public void handleBtn(View view) {
		switch (view.getId()) {
		case R.id.refresh_btn:
			getTimeLine(1);
			break;
		case R.id.home_btn:
			startActivity(new Intent(this, Face.class));
			break;
		case R.id.tweet_btn:
			if (mUpdateView == null) {
				ViewStub tmp = (ViewStub) findViewById(R.id.post_message);
				mUpdateView = (View) tmp.inflate();
				mUpdate_edit = (EditText) mUpdateView.findViewById(R.id.post_edit);
				mUpdate_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						Log.e(TAG, "onFocuseChange");
						if (hasFocus) {
							Log.e(TAG,"get Focuse");
							InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							if (imm != null) {
								
								imm.showSoftInput(mUpdate_edit, InputMethodManager.SHOW_FORCED);
							}
						}else{
							Log.e(TAG,"lost Focuse");
							if(DirectMessages.this.getCurrentFocus()!=null){
								Log.e(TAG,"hideSoftInputFromWindow");
							((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(DirectMessages.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
							}
						}
					}
				});

				Button btn = (Button) mUpdateView.findViewById(R.id.update_button);
				btn.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						Animation anim = AnimationUtils.loadAnimation(DirectMessages.this, R.anim.update_view_out);
						anim.setAnimationListener(new AnimationListener() {

							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub

							}

							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							public void onAnimationEnd(Animation animation) {
								Log.e(TAG, "onAnimationEnd");
								mUpdate_edit.setText("");
								if(DirectMessages.this.getCurrentFocus()!=null){
									Log.e(TAG,"hideSoftInputFromWindow");
								((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(DirectMessages.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
								}
								mUpdateView.setVisibility(View.GONE);
							}
						});
						mUpdateView.startAnimation(anim);

					}
				});
				btn = (Button) mUpdateView.findViewById(R.id.cancel_button);
				btn.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						Animation anim = AnimationUtils.loadAnimation(DirectMessages.this, R.anim.update_view_out);
						anim.setAnimationListener(new AnimationListener() {

							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub

							}

							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							public void onAnimationEnd(Animation animation) {
								Log.e(TAG, "onAnimationEnd");
								mUpdate_edit.setText("");
								if(DirectMessages.this.getCurrentFocus()!=null){
									Log.e(TAG,"hideSoftInputFromWindow");
								((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(DirectMessages.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
								}
								mUpdateView.setVisibility(View.GONE);
							}
						});
						mUpdateView.startAnimation(anim);

					}
				});
			}
			mUpdateView.setVisibility(View.VISIBLE);
			mUpdate_edit.requestFocus();
			mUpdateView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.update_view_in));
			break;
		}
	}

}
