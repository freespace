package com.weibo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weibo.util.AsyncImageLoader.ImageCallback;



public class Util {
	
	private AsyncImageLoader mImageLoader;
	private static final String TAG= "Util";
	public static String getPastTime(Date createDate) {

		Date currentTime = Calendar.getInstance().getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long between = (currentTime.getTime() - createDate.getTime()) / 1000;// 除以1000是为了转换成秒
		long day = between / (24 * 3600);
		long hour = between % (24 * 3600) / 3600;
		long minute = between % 3600 / 60;
		// System.out.println(""+day+"天"+hour+"小时"+min+"分"+s+"秒");
		String time = "";
		if (day > 0) {
			time = "" + day + "天";
		} else if (hour > 0) {
			time = hour + "小时";
		} else if (minute > 0) {
			time = minute + "分钟";
		}else{
			time = "1分钟";
		}
		time = time + "前";
		return time;
	}
	
	public static void addLink(TextView view, boolean isTitle) {
		Pattern pattern;
		if (isTitle) {
			pattern = Pattern.compile("@\\w*");
		} else {
			pattern = Pattern
					.compile("@[[\\u4e00-\\u9fa5][A-Z][a-z][0-9][_-]]+");
		}
		Linkify.addLinks(view, pattern, "user://UserActivity?user_name=");
		Linkify.addLinks(view, Patterns.WEB_URL,"http://");
		view.setLinkTextColor(0xff416BC0);
		CharSequence c = view.getText();
		// Spannable s = (Spannable) view.getText();
		if (c instanceof Spannable) {
			Spannable s = (Spannable) c;
			URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
			for (URLSpan span : spans) {
				int start = s.getSpanStart(span);
				int end = s.getSpanEnd(span);
				s.removeSpan(span);
				span = new URLSpanNoUnderline(span.getURL());
				s.setSpan(span, start, end, 0);
			}
			view.setText(s);
		}
		view.setMovementMethod(LinkMovementMethod.getInstance());
	}
	
	public static Drawable loadImage(String key, String url, Context context, final BaseAdapter adapter) {
		if (AsyncImageLoader.getInstance().getImageCache().containsKey(key)) {
			Drawable face = AsyncImageLoader.getInstance().getImageCache().get(key);
			if (face != null)
				return face;
			else {
				Log.e(TAG, "load image from file");
				face = Drawable.createFromPath(AsyncImageLoader.IMAGEDATAPATH + "/"
						+ key);
				return face;
			}
		} else {

			AsyncImageLoader.getInstance().loadDrawable(context, url, new ImageCallback() {
				public void imageLoaded(Drawable imageDrawable, String imageUrl) {
					adapter.notifyDataSetChanged();
				}
				 public void imageLoaded(Drawable imageDrawable, String imageUrl,int key){
				 }
			});
			return null;

		}
	}
	
	
	
	public static Drawable loadImage(String key, String url, Context context, final Handler handler,final int what) {
		if (AsyncImageLoader.getInstance().getImageCache().containsKey(key)) {
			Drawable face = AsyncImageLoader.getInstance().getImageCache().get(key);
			if (face != null)
				return face;
			else {
				face = Drawable.createFromPath(AsyncImageLoader.IMAGEDATAPATH + "/"
						+ key);
				return face;
			}
		} else {

			AsyncImageLoader.getInstance().loadDrawable(context, url, new ImageCallback() {
				public void imageLoaded(Drawable imageDrawable, String imageUrl) {
				}
				 public void imageLoaded(Drawable imageDrawable, String imageUrl,int key){
					 handler.obtainMessage(key, imageDrawable).sendToTarget();
				 }
			},what);
			return null;

		}
	}
}
