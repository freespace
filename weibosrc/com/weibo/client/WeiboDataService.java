
package com.weibo.client;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.weibo.net.AccessToken;
import com.weibo.net.WeiboException;

public class WeiboDataService extends Service {

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */

    private boolean isWeiboInit = false;

    private static final int INIT = 0;

    public class LocalBinder extends Binder {
        WeiboDataService getService() {
            return WeiboDataService.this;
        }
    }

    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return mBinder;
    }

    public void init() {

    }

    public void reflash() {

    }

    public void pull(Paging paging) {

    }

    @Override
    public void onCreate() {
        mHandler.obtainMessage(INIT).sendToTarget();
        super.onCreate();
    }

    public boolean isWeiboInit() {
        return isWeiboInit;
    }

    public void setWeiboInit(boolean isWeiboInit) {
        this.isWeiboInit = isWeiboInit;
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    SharedPreferences user = getSharedPreferences("oauth", 0);
                    String oauth_token = user.getString("oauth_token", "");
                    String oauth_token_secret = user.getString("oauth_token_secret", "");
                    if (oauth_token.length() > 0 && oauth_token_secret.length() > 0) {
                        AccessToken accessToken = new AccessToken(oauth_token,
                                Weibo.CONSUMER_SECRET);
                        accessToken.setExpiresIn(oauth_token_secret);
                        Weibo.getInstance().setAccessToken(accessToken);
                        Weibo.getInstance().setContext(getBaseContext());
                        Weibo.setUid(user.getString("uid", ""));
                        try {
                            Weibo.getInstance().getOAuthUid();
                        } catch (WeiboException e) {
                            isWeiboInit = false;
                            e.printStackTrace();
                        }
                        isWeiboInit = true;
                    }
                    break;
            }
        }

    };
    

}
