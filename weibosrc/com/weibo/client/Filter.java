
package com.weibo.client;

public class Filter {
    /**
     * 作者筛选类型，0：全部、1：我关注的人、2：陌生人，默认为0。
     * @author Administrator
     *
     */
    public enum FilterByAuthor {
        ALL, FRIENDS, STRANGER
    }
    /**
     * 来源筛选类型，0：全部、1：来自微博、2：来自微群，默认为0。
     * @author Administrator
     *
     */
    public enum FilterBySource {
        ALL, WEIBO, WEIQUN
    }
    /**
     * 原创筛选类型，0：全部微博、1：原创的微博，默认为0。
     * @author Administrator
     *
     */
    public enum FilterByType {
        ALL, ORIGINAL
    }
    /**
     * 是否在转发的同时发表评论，0：否、1：评论给当前微博、2：评论给原微博、3：都评论，默认为0 。
     * @author Administrator
     *
     */
    public enum IsComment {
        NOT, CURRENT,ORIGINAL,ALL
    }
    
    
}
