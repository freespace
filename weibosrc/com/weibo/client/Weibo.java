
package com.weibo.client;

import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weibo.client.Filter.FilterByAuthor;
import com.weibo.client.Filter.FilterBySource;
import com.weibo.client.Filter.FilterByType;
import com.weibo.client.Filter.IsComment;
import com.weibo.json.Annotation;
import com.weibo.json.Comment;
import com.weibo.json.Comments;
import com.weibo.json.Favorites;
import com.weibo.json.Reposts;
import com.weibo.json.Statuses;
import com.weibo.json.TimeLine;
import com.weibo.json.User;
import com.weibo.net.AccessToken;
import com.weibo.net.Oauth2AccessToken;
import com.weibo.net.RequestToken;
import com.weibo.net.Token;
import com.weibo.net.Utility;
import com.weibo.net.WeiboDialogListener;
import com.weibo.net.WeiboException;
import com.weibo.net.WeiboParameters;

public class Weibo {

    private static Weibo mWeiboInstance = null;

    private com.weibo.net.Weibo mWeibo;

    private static String APP_KEY = "";

    private static String APP_SECRET = "";

    public static final String CONSUMER_KEY = "222764142";// 替换为开发者的appkey，例如"1646212960";
    public static final String CONSUMER_SECRET = "087c31deb39cee8622248b3be8150e40";// 替换为开发者的appkey，例如"94098772160b6f8ffc1315374d8861f9";

    private Context mContext;
    
    private static String mUid;

    private Weibo() {
        if (mWeibo == null) {
            mWeibo = com.weibo.net.Weibo.getInstance();
        }
    }

    public synchronized static Weibo getInstance() {
        if (mWeiboInstance == null) {
            mWeiboInstance = new Weibo();
        }
        return mWeiboInstance;
    }

    // 设置accessToken
    public void setAccessToken(AccessToken token) {
        mWeibo.setAccessToken(token);
    }

    public Token getAccessToken() {
        return mWeibo.getAccessToken();
    }

    public void setupConsumerConfig(String consumer_key, String consumer_secret) {
        APP_KEY = consumer_key;
        APP_SECRET = consumer_secret;
        mWeibo.setupConsumerConfig(consumer_key, consumer_secret);
    }

    public static String getAppKey() {
        return APP_KEY;
    }

    public static String getAppSecret() {
        return Weibo.APP_SECRET;
    }

    public void setRequestToken(RequestToken token) {
        mWeibo.setRequestToken(token);
    }

    public static String getSERVER() {
        return com.weibo.net.Weibo.SERVER;
    }

    public static void setSERVER(String sERVER) {
        com.weibo.net.Weibo.SERVER = sERVER;
    }

    // 设置oauth_verifier
    public void addOauthverifier(String verifier) {
        mWeibo.addOauthverifier(verifier);
    }

    public String getRedirectUrl() {
        return mWeibo.getRedirectUrl();
    }

    public void setRedirectUrl(String mRedirectUrl) {
        mWeibo.setRedirectUrl(mRedirectUrl);
    }
    
    public static String getUid() {
        return mUid;
    }

    public static void setUid(String mUid) {
        Weibo.mUid = mUid;
    }

    /**
     * Requst sina weibo open api by get or post
     * 
     * @param url Openapi request URL.
     * @param params http get or post parameters . e.g.
     *            gettimeling?max=max_id&min=min_id max and max_id is a pair of
     *            key and value for params, also the min and min_id
     * @param httpMethod http verb: e.g. "GET", "POST", "DELETE"
     * @throws IOException
     * @throws MalformedURLException
     * @throws WeiboException
     */
    public String request(Context context, String url, WeiboParameters params, String httpMethod,
            Token token) throws WeiboException {
        return mWeibo.request(context, url, params, httpMethod, token);
    }

    /**/
    public RequestToken getRequestToken(Context context, String key, String secret,
            String callback_url) throws WeiboException {
        return mWeibo.getRequestToken(context, key, secret, callback_url);
    }

    public AccessToken generateAccessToken(Context context, RequestToken requestToken)
            throws WeiboException {

        return mWeibo.generateAccessToken(context, requestToken);
    }

    public AccessToken getXauthAccessToken(Context context, String app_key, String app_secret,
            String usrname, String password) throws WeiboException {

        return mWeibo.getXauthAccessToken(context, app_key, app_secret, usrname, password);
    }

    /**
     * 获取Oauth2.0的accesstoken
     * https://api.weibo.com/oauth2/access_token?client_id=YOUR_CLIENT_ID&
     * client_secret=YOUR_CLIENT_SECRET&grant_type=password&redirect_uri=
     * YOUR_REGISTERED_REDIRECT_URI&username=USER_NAME&pasword=PASSWORD
     * 
     * @param context
     * @param app_key
     * @param app_secret
     * @param usrname
     * @param password
     * @return
     * @throws WeiboException
     */
    public Oauth2AccessToken getOauth2AccessToken(Context context, String app_key,
            String app_secret, String usrname, String password) throws WeiboException {

        return mWeibo.getOauth2AccessToken(context, app_key, app_secret, usrname, password);
    }

    /**
     * User-Agent Flow
     * 
     * @param activity
     * @param listener 授权结果监听器
     */
    public void authorize(Activity activity, final WeiboDialogListener listener) {
        mWeibo.authorize(activity, listener);
    }

    public void dialog(Context context, WeiboParameters parameters,
            final WeiboDialogListener listener) {
        mWeibo.dialog(context, parameters, listener);
    }

    public boolean isSessionValid() {
        return mWeibo.isSessionValid();
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 获取当前登录用户及其所关注用户的最新微博消息。<br/>
     * 和用户登录 http://t.sina.com.cn 后在“我的首页”中看到的内容相同。
     * 
     * @param paging 相关分页参数
     * @return list of the Friends Timeline
     * @throws WeiboException when Weibo service or network is unavailable
     * @throws weibo4android.WeiboException
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/friends_timeline">
     *      statuses/friends_timeline </a>
     */
    public TimeLine getFriendsTimeline(Paging paging) throws WeiboException {
        // return Status.constructStatuses(get(getBaseURL() +
        // "statuses/friends_timeline.json",null, paging, true));
        String url = com.weibo.net.Weibo.SERVER + "statuses/friends_timeline.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("page", paging.getPage() + "");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, "GET", mWeibo.getAccessToken());
        TimeLine timeline = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            timeline = mapper.readValue(tmp, TimeLine.class);
            Log.e("Weibo", timeline.getStatuses().size() + "");
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return timeline;
    }

    /**
     * 收藏一条微博消息
     * 
     * @param id the ID of the status to favorite
     * @return Status
     * @throws WeiboException when Weibo service or network is unavailable
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Favorites/create">favorites/create
     *      </a>
     */
    public Favorites createFavorite(long id, boolean needResult) throws WeiboException {
        // eturn new Status(http.post(getBaseURL() + "favorites/create/" + id +
        // ".json", true));
        String url = com.weibo.net.Weibo.SERVER + "favorites/create.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("id", id + "");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        Favorites favorites = null;
        if (needResult) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                favorites = mapper.readValue(
                        mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_POST,
                                mWeibo.getAccessToken()), Favorites.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return favorites;

    }

    /**
     * 根据ID获取单条微博消息，以及该微博消息的作者信息。
     * 
     * @param id 要获取的微博消息ID
     * @return 微博消息
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/show">statuses/show
     *      </a>
     */
    public Statuses showStatus(String id) throws WeiboException {

        String url = com.weibo.net.Weibo.SERVER + "statuses/show.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("id", id);
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET,
                mWeibo.getAccessToken());
        Statuses statuses = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            statuses = mapper.readValue(tmp, Statuses.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return statuses;
    }

    /**
     * 根据ID获取单条微博消息，以及该微博消息的作者信息。
     * 
     * @param id 要获取的微博消息ID
     * @return 微博消息
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/show">statuses/show
     *      </a>
     */
    public Statuses showStatus(long id) throws WeiboException {
        return showStatus(Long.toString(id));
    }

    /**
     * 根据微博消息ID返回某条微博消息的20条评论列表
     * 
     * @param id specifies the ID of status
     * @return a list of comments objects
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/comments">Statuses/comments</a>
     */
    public Comments getComments(String id) throws WeiboException {
        return getComments(id, null,FilterByAuthor.ALL);
    }

    /**
     * 根据微博消息ID返回某条微博消息的n评论列表
     * 
     * @param id specifies the ID of status
     * @return a list of comments objects
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/comments">Statuses/comments</a>
     */
    public Comments getComments(String id, Paging paging,FilterByAuthor filter_by_author) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "comments/show.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("id", id);
        if (paging != null) {
            bundle.add("page", paging.getPage() + "");
            bundle.add("since_id", paging.getSinceId() + "");
            bundle.add("max_id", paging.getMaxId() + "");
            bundle.add("count", paging.getCount() + "");
            bundle.add("filter_by_author", filter_by_author + "");
        }
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET,
                mWeibo.getAccessToken());
        Comments comments = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            comments = mapper.readValue(tmp, Comments.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return comments;
    }

    /**
     * 返回一条原创微博的最新20条转发微博信息本接口无法对非原创微博进行查询。
     * 
     * @param id specifies the id of original status.
     * @return a list of statuses object
     * @throws WeiboException
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/repost_timeline">Statuses/repost_timeline</a>
     */
    public Reposts getReposttimeline(String id) throws WeiboException {
        return getReposttimeline(id, null,FilterByAuthor.ALL);
    }

    /**
     * 返回一条原创微博的最新n条转发微博信息,本接口无法对非原创微博进行查询。
     * 
     * @param id specifies the id of original status.
     * @return a list of statuses object
     * @throws WeiboException
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/repost_timeline">Statuses/repost_timeline</a>
     */
    public Reposts getReposttimeline(String id, Paging paging,FilterByAuthor filter_by_author) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "statuses/repost_timeline.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("id", id);
        if (paging != null) {
            bundle.add("page", paging.getPage() + "");
            bundle.add("since_id", paging.getSinceId() + "");
            bundle.add("max_id", paging.getMaxId() + "");
            bundle.add("count", paging.getCount() + "");
        }
        bundle.add("filter_by_author", filter_by_author+"");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET,
                mWeibo.getAccessToken());
        Reposts reposts = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            reposts = mapper.readValue(tmp, Reposts.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return reposts;
    }

    /**
     * 对一条微博信息进行评论
     * 
     * @param 评论内容。必须做URLEncode,信息内容不超过140个汉字。
     * @param id 要评论的微博消息ID
     * @param cid 要回复的评论ID,可以为null.如果id及cid不存在，将返回400错误
     *            </br>如果提供了正确的cid参数，则该接口的表现为回复指定的评论。<br/>
     *            此时id参数将被忽略。<br/>
     *            即使cid参数代表的评论不属于id参数代表的微博消息，通过该接口发表的评论信息直接回复cid代表的评论。
     * @return the comment object
     * @throws WeiboException
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/comment">Statuses/comment</a>
     */
    public Comment createComment(String contents, String id, String comment_ori, boolean needResult)
            throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "comments/create.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("id", id);
        bundle.add("comment", contents);
        bundle.add("comment_ori", comment_ori);
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_POST,
                mWeibo.getAccessToken());
        Comment comment = null;
        if (needResult) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                comment = mapper.readValue(tmp, Comment.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return comment;
    }

    /**
     * 删除评论。注意：只能删除登录用户自己发布的评论，不可以删除其他人的评论。
     * 
     * @param statusId 欲删除的评论ID
     * @return the deleted status
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/comment_destroy">statuses/comment_destroy
     *      </a>
     */
    public Comment destroyComment(String commentId, boolean needResult) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "comments/destroy.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("cid", commentId);
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_POST,
                mWeibo.getAccessToken());
        Comment comment = null;
        if (needResult) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                comment = mapper.readValue(tmp, Comment.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return comment;
    }

    /**
     * 发布一条微博信息
     * 
     * @param status 要发布的微博消息文本内容
     * @param inReplyToStatusId 要转发的微博消息ID
     * @return the latest status
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a
     *      href="http://open.t.sina.com.cn/wiki/index.php/Statuses/update">statuses/update
     *      </a>
     */
    public Statuses updateStatus(String status, String lat, String lng, Annotation annotation,
            boolean needResult) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "statuses/update.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("status", status);
        if(lat!=null&&lng!=null){
        bundle.add("lat", lat);
        bundle.add("lng", lng);
        }
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_POST,
                mWeibo.getAccessToken());
        Statuses statuses = null;
        if (needResult) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                statuses = mapper.readValue(tmp, Statuses.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return statuses;
    }
    
    /**
     * 转发微博
     * @param sid 要转发的微博ID
     * @param status 添加的转发文本。必须做URLEncode,信息内容不超过140个汉字。如不填则默认为“转发微博”。
     * @return a single status
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     */
    public Statuses repost(String sid,String status) throws WeiboException {
        return repost(sid, status,IsComment.NOT,false);
    }
    /**
     * 转发微博
     * @param sid 要转发的微博ID
     * @param status 添加的转发文本。必须做URLEncode,信息内容不超过140个汉字。如不填则默认为“转发微博”。
     * @param isComment 是否在转发的同时发表评论，0：否、1：评论给当前微博、2：评论给原微博、3：都评论，默认为0 。
     * @return a single status
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     */
    public Statuses repost(String sid,String status,IsComment isComment,boolean needResult) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "statuses/repost.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("status", status);
        bundle.add("id", sid);
        bundle.add("is_comment", isComment+"");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_POST,
                mWeibo.getAccessToken());
        Statuses statuses = null;
        if (needResult) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                statuses = mapper.readValue(tmp, Statuses.class);
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return statuses;
    }
    /**
     * 返回最新提到登录用户的微博消息（即包含@username的微博消息）
     * @param paging 分页数据
     * @return the 20 most recent replies
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a href="http://open.t.sina.com.cn/wiki/index.php/Statuses/mentions">Statuses/mentions </a>
     */
    public TimeLine getMentions() throws WeiboException {
        return getMentions(null,FilterByAuthor.ALL,FilterBySource.ALL,FilterByType.ALL);
    }

    /**
     * 返回最新提到登录用户的微博消息（即包含@username的微博消息）
     * @param paging 分页数据
     * @return the 20 most recent replies
     * @throws WeiboException when Weibo service or network is unavailable
     * @since Weibo4J 1.2.1
     * @see <a href="http://open.t.sina.com.cn/wiki/index.php/Statuses/mentions">Statuses/mentions </a>
     */
    public TimeLine getMentions(Paging paging,FilterByAuthor filter_by_author,FilterBySource filter_by_source,FilterByType filter_by_type) throws WeiboException {
        String url = com.weibo.net.Weibo.SERVER + "statuses/friends_timeline.json";
        WeiboParameters bundle = new WeiboParameters();
        if(paging!=null){
        bundle.add("page", paging.getPage() + "");
        bundle.add("since_id", paging.getSinceId()+"");
        bundle.add("max_id", paging.getMaxId() + "");
        bundle.add("count", paging.getCount() + "");
        }
        bundle.add("filter_by_author", filter_by_author + "");
        bundle.add("filter_by_source", filter_by_source + "");
        bundle.add("filter_by_type", filter_by_type + "");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET, mWeibo.getAccessToken());
        TimeLine timeline = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            timeline = mapper.readValue(tmp, TimeLine.class);
            Log.e("Weibo", timeline.getStatuses().size() + "");
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return timeline;
    }
    /**
     * 根据用户ID获取用户信息
     * @param uid 需要查询的用户ID。
     * @return 用户信息
     * @throws WeiboException
     */
    public User userShow(long uid) throws WeiboException{
        String url = com.weibo.net.Weibo.SERVER + "users/show.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("uid", uid + "");
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET, mWeibo.getAccessToken());
        User user = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            user = mapper.readValue(tmp, User.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return user;
    }
    
    /**
     * 根据用户ID获取用户信息
     * @param screen_name 需要查询的用户昵称。
     * @return 用户信息
     * @throws WeiboException
     */
    public User userShow(String screen_name) throws WeiboException{
        String url = com.weibo.net.Weibo.SERVER + "statuses/friends_timeline.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add("screen_name", screen_name);
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET, mWeibo.getAccessToken());
        User user = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            user = mapper.readValue(tmp, User.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return user;
    }
    
    /**
     * OAuth授权之后，获取授权用户的UID
     * @return UID
     * @throws WeiboException
     */
    public long getOAuthUid() throws WeiboException{
        String url = com.weibo.net.Weibo.SERVER + "account/get_uid.json";
        WeiboParameters bundle = new WeiboParameters();
        bundle.add(com.weibo.net.Weibo.TOKEN, mWeibo.getAccessToken().getToken());
        String tmp = mWeibo.request(mContext, url, bundle, Utility.HTTPMETHOD_GET, mWeibo.getAccessToken());
        Long uid = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            uid = mapper.readValue(tmp, Long.class);
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return uid.longValue();
    }

   
}
