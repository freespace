package com.weibo.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.weibo.sina.ImageViewActivity;

public class LinkImageView extends ImageView implements OnClickListener{

	private String mUrl;
	private Context mContext;
	public LinkImageView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LinkImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnClickListener(this);
		mContext = context;
	}

	public LinkImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public String getUrl() {
		return mUrl;
	}

	public void setUrl(String mUrl) {
		this.mUrl = mUrl;
	}

	public void onClick(View v) {
		Intent intent = new Intent(mContext,ImageViewActivity.class);
		intent.putExtra("url", mUrl);
		mContext.startActivity(intent);
	}
	
	

}
