package com.weibo.view;

import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.weibo.db.HomeTable;
import com.weibo.sina.R;
import com.weibo.util.MD5;
import com.weibo.util.Util;

public class StatusCursorAdapter extends CursorAdapter {
    private static final String TAG = "StatusCursorAdapter";
	private int mLayout;
	public StatusCursorAdapter(Context context, Cursor c,final int layout) {
		super(context, c);
		mLayout = layout;
		
	}
	

	public StatusCursorAdapter(Context context, Cursor c, boolean autoRequery,final int layout) {
		super(context, c, autoRequery);
		mLayout = layout;
	}


	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
	    //Log.e(TAG, "get new View");
		LayoutInflater inflater = (LayoutInflater) context
		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(mLayout, parent, false);
		StatusObj tag = new StatusObj();
		view.setTag(tag);
		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		//long start = System.currentTimeMillis();
		StatusView statuView = (StatusView)view;
		StatusObj tag =(StatusObj) statuView.getTag();
		tag.clear();
		tag.setId(cursor.getLong(cursor.getColumnIndex(HomeTable._ID)));
		tag.setRt_id(cursor.getLong(cursor.getColumnIndex(HomeTable.RTROOTID)));
		String tmpString="";
		view.setTag(tag);
		//TODO: view.findViewById用的时间很长， 这里可以把View修改成自定义的类提供get方法提高性能。
		TextView tmp = statuView.getUser_name();
		tmpString = cursor.getString(cursor.getColumnIndex(HomeTable.NICK));
		tmp.setText("@" +tmpString);
		tag.setNick(tmpString);
		//Log.e(TAG, "user_name = "+tmp.getText());
		Util.addLink(tmp, true);
		tmp = statuView.getPost_time();

		
		
		tmp.setText(Util.getPastTime(new Date(cursor.getLong(cursor.getColumnIndex(HomeTable.TIME)))));
		tmp = statuView.getContent();
		tmp.setTag(tag);
		tmpString=cursor.getString(cursor.getColumnIndex(HomeTable.CONTENT));
//		tmpString = tmpString+ " "+cursor.getString(cursor.getColumnIndex(HomeTable.LONGITUDE));
//		tmpString = tmpString+ " "+cursor.getString(cursor.getColumnIndex(HomeTable.LATITUDE));
		tmp.setText(tmpString);
		tag.setContent(tmpString);
		Util.addLink(tmp, true);
		
//		long start1=System.currentTimeMillis();
//		long duration = start1 - start;
//		Log.e(TAG, "duration = "+duration);
		
		
		ImageView imageView =null;
		Drawable imageDrawable=null;
		imageView = statuView.getFace();
		String profileImage = cursor.getString(cursor.getColumnIndex(HomeTable.PORTRAIT));
		tag.setFace_url(profileImage);
		String key = MD5.getMD5(profileImage.getBytes());
		imageDrawable = Util.loadImage(key, profileImage,context,this);
		if (imageDrawable != null)
			imageView.setBackgroundDrawable(imageDrawable);
		else {
			imageView.setBackgroundResource(R.drawable.face);
		}
//		long start2 = System.currentTimeMillis();
//		duration = start2 - start1;
//		Log.e(TAG, "duration = "+duration);
		//Log.e(TAG, "rtrootid = "+cursor.getLong(cursor.getColumnIndex(HomeTable.RTROOTID)));
		View t = view.findViewById(R.id.rt_layout);
		long rtrootid = cursor.getLong(cursor.getColumnIndex(HomeTable.RTROOTID));
		if(rtrootid!=0){
			t.setVisibility(View.VISIBLE);
			tmp = statuView.getRt_nick();
			tmpString=cursor.getString(cursor.getColumnIndex(HomeTable.RTROOTNICK));
			tmp.setText("@" +tmpString);
			tag.setRt_nick(tmpString);
			tag.setRt_face_url(cursor.getString(cursor.getColumnIndex(HomeTable.RTPORTRAIT)));
			Util.addLink(tmp, false);
			tmp = statuView.getRt_reason();
			tmpString=cursor.getString(cursor.getColumnIndex(HomeTable.RTREASON));
			tmp.setText(tmpString);
			tag.setRt_reason(tmpString);
			Util.addLink(tmp, false);
			tmp.setTag(tag);
		}else {
			t.setVisibility(View.GONE);
			
		}
		
//		duration = System.currentTimeMillis() - start2;
//		Log.e(TAG, "duration = "+duration);
		
		String pic = cursor.getString(cursor.getColumnIndex(HomeTable.PIC));
		//Log.e(TAG, "pic = "+pic);
		LinkImageView linkImageView = statuView.getImage();
		if (pic!=null &&pic.trim().length()>0) {
			linkImageView.setVisibility(View.VISIBLE);
			
			linkImageView.setUrl(cursor.getString(cursor.getColumnIndex(HomeTable.OPIC)));
			key = MD5.getMD5(pic.getBytes());
			imageDrawable = Util.loadImage(key, pic,context,this);
			if (imageDrawable != null)
				linkImageView.setImageDrawable(imageDrawable);
			else{
				linkImageView.setImageResource(R.drawable.preview_pic_loading);
			}
		} else {
			linkImageView.setVisibility(View.GONE);
		}
		
		
//		duration = System.currentTimeMillis() - start;
//		Log.e(TAG, "duration = "+duration+"cursor="+cursor.getPosition());
	}
	
	public static class StatusObj {
		private long id;
		private long rt_id;
		private String nick;
		private String rt_nick;
		private String content;
		private String rt_reason;
		private String face_url;
		private String rt_face_url;
		
		public void clear(){
			id=0;
			rt_id=0;
			nick=null;
			rt_nick=null;
			content=null;
			rt_reason=null;
			face_url=null;
			rt_face_url=null;
		}
		
		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getNick() {
			return nick;
		}

		public void setNick(String nick) {
			this.nick = nick;
		}

		public String getRt_nick() {
			return rt_nick;
		}

		public void setRt_nick(String rt_nick) {
			this.rt_nick = rt_nick;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getRt_reason() {
			return rt_reason;
		}

		public void setRt_reason(String rt_reason) {
			this.rt_reason = rt_reason;
		}

		public long getRt_id() {
			return rt_id;
		}

		public void setRt_id(long rt_id) {
			this.rt_id = rt_id;
		}

		public String getFace_url() {
			return face_url;
		}

		public void setFace_url(String face_url) {
			this.face_url = face_url;
		}

		public String getRt_face_url() {
			return rt_face_url;
		}

		public void setRt_face_url(String rt_face_url) {
			this.rt_face_url = rt_face_url;
		}
		
		

		
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
