package com.weibo.view;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.weibo.json.Statuses;
import com.weibo.sina.R;
import com.weibo.util.MD5;
import com.weibo.util.Util;

public class StatusAdapter extends ArrayAdapter<Statuses> {
	private List<Statuses> mFriendsTimeline;
	private int mLayout;
	private Context mContext;
	static final String TAG = "StatusAdapter";
	private ListView mListView;

	public StatusAdapter(Context context, int resource, List<Statuses> objects,
			ListView view) {
		super(context, resource, objects);
		mFriendsTimeline = objects;
		mLayout = resource;
		mContext = context;
		mListView = view;
		
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		Statuses data = null;

		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			row = inflater.inflate(mLayout, parent, false);
			// data =mFriendsTimeline.get(position);
			// row.setTag(data);
		} else {
			// data = (Status) row.getTag();
		}
		data = mFriendsTimeline.get(position);
		//Log.e(TAG,"Mid = "+data.getMid()+"id = "+data.getId()+" ");
		TextView tmp = (TextView) row.findViewById(R.id.user_name);
		tmp.setText("@" + data.getUser().getName());
		Util.addLink(tmp, true);

		tmp = (TextView) row.findViewById(R.id.post_time);

		tmp.setText(Util.getPastTime(new Date(data.getCreated_at())));

		tmp = (TextView) row.findViewById(R.id.content);

		tmp.setText(data.getText());
		ImageView imageView =null;
		String key =null;
		Drawable imageDrawable=null;
		Util.addLink(tmp, false);
		if (data.getRetweeted_status() != null) {
			View t = row.findViewById(R.id.rt_layout);
			t.setVisibility(View.VISIBLE);
			tmp = (TextView) row.findViewById(R.id.rt_reason);
			tmp.setText(data.getRetweeted_status().getUser().getName() + "\n"
					+ data.getRetweeted_status().getText());
			Util.addLink(tmp, false);
			tmp.setMovementMethod(LinkMovementMethod.getInstance());
//			imageView = (ImageView) row.findViewById(R.id.rt_image);
//			if (data.getRetweeted_status().getThumbnail_pic().length()>0) {
//				imageView.setImageResource(R.drawable.preview_pic_loading);
//				imageView.setVisibility(View.VISIBLE);
//				key = MD5.getMD5(data.getRetweeted_status().getThumbnail_pic().getBytes());
//				imageDrawable = Util.loadImage(key, data.getRetweeted_status().getThumbnail_pic(),getContext(),this);
//				if (imageDrawable != null)
//					imageView.setImageDrawable(imageDrawable);
//			} else {
//				imageView.setVisibility(View.GONE);
//			}
		} else {
			View t = row.findViewById(R.id.rt_layout);
			t.setVisibility(View.GONE);
		}

		imageView = (ImageView) row.findViewById(R.id.face);

		// face.setTag(data.getUser().getProfileImageURL().toString());

		// if(data.getThumbnail_pic()!=null){
		// //Log.e(TAG, data.getThumbnail_pic());
		// //Log.e(TAG, "MD5="+MD5.getMD5(data.getThumbnail_pic().getBytes()));
		// }
		key = MD5.getMD5(data.getUser().getProfile_image_url().getBytes());
		imageDrawable = Util.loadImage(key, data.getUser().getProfile_image_url(),getContext(),this);
		if (imageDrawable != null)
			imageView.setBackgroundDrawable(imageDrawable);
		else {
			imageView.setBackgroundResource(R.drawable.face);
		}

		imageView = (ImageView) row.findViewById(R.id.image);
		if (data.getThumbnail_pic()!=null&&data.getThumbnail_pic().length()>0) {
			imageView.setImageResource(R.drawable.preview_pic_loading);
			imageView.setVisibility(View.VISIBLE);
			key = MD5.getMD5(data.getThumbnail_pic().getBytes());
			imageDrawable = Util.loadImage(key, data.getThumbnail_pic(),getContext(),this);
			if (imageDrawable != null)
				imageView.setImageDrawable(imageDrawable);
		} else {
			imageView.setVisibility(View.GONE);
		}
		
		// if (cachedImage == null) {
		// face.setImageResource(R.drawable.face);
		// }else{
		// face.setImageDrawable(cachedImage);
		// }
		// row.setPressed(true);
		return (row);
	}
	

	
}
