package com.weibo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StatusView extends LinearLayout {

	private TextView user_name;
	private TextView post_time;
	private ImageView face;
	private TextView content;
	private TextView rt_nick;
	private TextView rt_reason;
	private LinkImageView image;

	public StatusView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		user_name = (TextView) ((LinearLayout)getChildAt(0)).getChildAt(0);
		post_time = (TextView) ((LinearLayout)getChildAt(0)).getChildAt(1);
		face = (ImageView)((LinearLayout)getChildAt(1)).getChildAt(0);
		content = (TextView) ((LinearLayout) ((LinearLayout)getChildAt(1)).getChildAt(1)).getChildAt(0);
		rt_nick = (TextView) ((LinearLayout) ((LinearLayout) ((LinearLayout) ((LinearLayout)getChildAt(1)).getChildAt(1)).getChildAt(1)).getChildAt(1)).getChildAt(0);
		rt_reason = (TextView) ((LinearLayout) ((LinearLayout) ((LinearLayout) ((LinearLayout)getChildAt(1)).getChildAt(1)).getChildAt(1)).getChildAt(1)).getChildAt(1);
		image = (LinkImageView) ((LinearLayout) ((LinearLayout) ((LinearLayout)getChildAt(1)).getChildAt(1)).getChildAt(2)).getChildAt(0);
		Log.e("StatusView","onFinishInflate");
		super.onFinishInflate();
	}

	public TextView getUser_name() {
		return user_name;
	}

	public void setUser_name(TextView user_name) {
		this.user_name = user_name;
	}

	public TextView getPost_time() {
		return post_time;
	}

	public void setPost_time(TextView post_time) {
		this.post_time = post_time;
	}

	public ImageView getFace() {
		return face;
	}

	public void setFace(ImageView face) {
		this.face = face;
	}

	public TextView getContent() {
		return content;
	}

	public void setContent(TextView content) {
		this.content = content;
	}

	public TextView getRt_nick() {
		return rt_nick;
	}

	public void setRt_nick(TextView rt_nick) {
		this.rt_nick = rt_nick;
	}

	public TextView getRt_reason() {
		return rt_reason;
	}

	public void setRt_reason(TextView rt_reason) {
		this.rt_reason = rt_reason;
	}

	public LinkImageView getImage() {
		return image;
	}

	public void setImage(LinkImageView image) {
		this.image = image;
	}
	
	

}
